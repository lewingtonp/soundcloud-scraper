# Components

## 1. Locator

Takes: 
1. user id
2. offset
3. app version
4. client id 

Gives:
1. Batch

Side Effects:
NONE

## 2. Assembler

Takes:
1. Batch

Gives:
NONE

Side Effects:
1. Saves every track to a file in a directory

## 4. Overseer

Takes:
1. Batch
2. Final Song link

Gives:
1. Indicator of whether we are finished


Side Effects:
NONE

# Datastructures

## 1. Track

- Link `string`
- Name `string`

## 2. Batch

- Tracks `List<Track>`
- Offset `int`
