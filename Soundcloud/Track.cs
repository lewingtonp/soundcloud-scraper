using System;
using System.Text.RegularExpressions;

namespace Soundcloud
{
    // Track represents a unique soundcloud track.
    public class Track
    {


        public int ID {get;}
        public string Name {get;}
        public string Link {get;}
        public Track(int id, string name, string link) {
            this.ID = id;
            this.Name = Regex.Replace(name, "[/~%&*|'\"\\\\]", "-");
            this.Link = link;
        }

        public Track(Raw.Track rawTrack) {
            this.ID = rawTrack.id;
            this.Name = rawTrack.title;
            this.Link = rawTrack.permalink_url;
        }
    }
}