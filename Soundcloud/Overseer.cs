using System;

namespace Soundcloud 
{
    public interface Overseer
    {
        bool FinalBatch(Batch batch);
    }
}