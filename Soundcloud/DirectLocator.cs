using System;
using System.Net.Http;
using System.Runtime.Serialization.Json;

namespace Soundcloud
{
    // DirectLocator locates tracks and their links by 
    // making http requests to soundcloud.
    public class DirectLocator : Locator
    {
        private int userID;
        private int appVersion;
        private string clientID;
        private HttpClient client;

        public DirectLocator(int userID, int appVersion, string clientID, HttpClient client)
        {
            this.userID = userID;
            this.clientID = clientID;
            this.appVersion = appVersion;
            this.client = client;
        }

        private string NextBatchURL(long offset)
        {
            return string.Format($"https://api-v2.soundcloud.com/users/{this.userID}/track_likes?offset={offset}&limit=2000&client_id={this.clientID}&app_version={this.appVersion}&app_locale=en");
        }

        public Batch NextBatch(long offset) 
        {
            var response = this.client.GetAsync(this.NextBatchURL(offset)).GetAwaiter().GetResult();
            response.EnsureSuccessStatusCode();

            var responseStream = response.Content.ReadAsStreamAsync().GetAwaiter().GetResult();

            DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(Raw.Batch));

            Raw.Batch rawBatch = (Raw.Batch)deserializer.ReadObject(responseStream);

            return new Batch(rawBatch);
        }
    }
}