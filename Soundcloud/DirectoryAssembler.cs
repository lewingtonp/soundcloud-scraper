using System;
using System.IO;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Soundcloud
{
    public class DirectoryAssembler : Assembler
    {
        private string directoryPath;
        private string clientID;
        private HttpClient client;
        public DirectoryAssembler(string directoryPath, string clientID, HttpClient client)
        {
            this.directoryPath = directoryPath;
            this.clientID = clientID;
            this.client = client;
        }

        // public void AssembleTracks(Batch batch)
        // {
        //     Task.WaitAll(this.assemblyTasks(batch.Tracks));
        // }

        public void AssembleTracks(Batch batch)
        {
            foreach (Track track in batch.Tracks)
            {
                this.assembleTrack(track).GetAwaiter().GetResult();
            }
        }

        private Task[] assemblyTasks(List<Track> tracks)
        {
            List<Task> tasks = new List<Task>{};

            foreach(Track track in tracks)
            {
                tasks.Add(this.assembleTrack(track));
            }

            return tasks.ToArray();
        }

        private async Task assembleTrack(Track track)
        {
            downloadURL url = this.downloadURL(track);

            if (url.isValid) 
            {
                var response = await this.client.GetAsync(url.URL);
                await this.saveToFile(track, response);
            } else {
                Console.WriteLine($"a valid download url could not be found for track ${track}");
            }
        }

        private async Task<Stream> saveToFile(Track track, HttpResponseMessage response)
        {
            using (Stream fileInput = File.OpenWrite($"{this.directoryPath}/{track.Name}.mp3"))
            using(var responseOutputTask = response.Content.ReadAsStreamAsync())
            {
                Stream responseOutput = await responseOutputTask;
                responseOutput.CopyTo(fileInput);
                return responseOutput;
            }
        }

        private string allDownloadsURL(int trackID)
        {
            return string.Format($"https://api.soundcloud.com/i1/tracks/{trackID}/streams?client_id={this.clientID}");
        }

        private downloadURL downloadURL(Track track)
        {
            Console.WriteLine(this.allDownloadsURL(track.ID));
            var response = this.client.GetAsync(this.allDownloadsURL(track.ID)).GetAwaiter().GetResult();

            var responseStream = response.Content.ReadAsStreamAsync().GetAwaiter().GetResult();

            DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(Raw.TrackDownloads));

            Raw.TrackDownloads rawDownloads = (Raw.TrackDownloads)deserializer.ReadObject(responseStream);

            return new downloadURL(rawDownloads.http_mp3_128_url);  
        }
    }
}