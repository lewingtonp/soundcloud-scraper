using System;
using System.Net.Http;

namespace Soundcloud
{
    public class Taker
    {
        private Locator locator;
        private Assembler assembler;

        public Taker(
            int userID, 
            int appVersion,
            string clientID,
            string savePath
        )
        {
            HttpClient client = new HttpClient();
            this.locator = new DirectLocator(userID, appVersion, clientID, client);
            this.assembler = new DirectoryAssembler(savePath, clientID, client);
        }   

        public void TakeUpTo(string targetPermalink)
        {
            Overseer overseer = new LinkMatchOverseer(targetPermalink);

            long offset = 0;
            while (true) {
                Batch nextBatch = this.locator.NextBatch(offset);

                this.assembler.AssembleTracks(nextBatch);

                if (overseer.FinalBatch(nextBatch))
                {
                    break;
                }
                offset = nextBatch.Offset;
            }
        }
    }
}