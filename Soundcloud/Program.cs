﻿using System;
using System.IO;
using System.Collections;

namespace Soundcloud
{
    class Program
    {
        static void Main(string[] args)
        {   
            Taker taker = new Taker(
                Constants.UserID,
                Constants.AppVersion,
                Constants.ClientID, 
                Constants.ProjectPath + "likes"
            );

            taker.TakeUpTo("https://soundcloud.com/blow-official/green-unicorn-1");
        }
    }
}
