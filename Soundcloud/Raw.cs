using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Soundcloud.Raw
{
    public class PublisherMetadata
    {
    }

    public class Format
    {
    }

    public class Transcoding
    {
    }

    public class Media
    {
        public IList<Transcoding> transcodings { get; set; }
    }

    [DataContract]
    public class User
    {

    }

    public class Track
    {
        public int id { get; set; }
        public string permalink_url { get; set; }
        public string title { get; set; }
    }

    public class Collection
    {
        public string kind { get; set; }
        public Track track { get; set; }
    }

    public class Batch
    {
        public IList<Collection> collection { get; set; }
        public string next_href { get; set; }
        public object query_urn { get; set; }
    }


        public class TrackDownloads
    {
        public string http_mp3_128_url { get; set; }
    }

}    