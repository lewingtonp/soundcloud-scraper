using System;

namespace Soundcloud
{
    interface Assembler
    {
        void AssembleTracks(Batch batch);
    }
}