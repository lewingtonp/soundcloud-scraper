using System;

namespace Soundcloud
{
    class downloadURL
    {
        public string URL {get;}
        public bool isValid {get;}

        public downloadURL(string url) 
        {
            Console.WriteLine(url);
            this.URL = url;
            this.isValid = url != null && url.Length > 40;
        }
    }
}