using System;

namespace Soundcloud
{
    interface Locator
    {
        Batch NextBatch(long offset);
    }
}