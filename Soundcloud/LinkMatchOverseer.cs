using System;

namespace Soundcloud
{
    // LinkMatchOverseer scans batches for the target 
    // track based on an exact string match of the track's
    // link.
    public class LinkMatchOverseer : Overseer
    {
        private string targetLink;

        public LinkMatchOverseer(string link)
        {
            this.targetLink = link;
        }

        public bool FinalBatch(Batch batch) 
        {
            if (batch.IsEmpty()) {
                return true;
            }

            foreach (var track in batch.Tracks)
            {
                if (track.Link == this.targetLink)
                {
                    return true;
                }
            }

            return false;
        }
    }
}