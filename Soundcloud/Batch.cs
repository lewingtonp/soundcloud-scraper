using System;
using System.Collections.Generic;

namespace Soundcloud
{
    // Batch represents a sequentially ordered batch of  
    // tracks retrived from soundcloud. 
    public class Batch
    {

        public List<Track> Tracks {get;}

        // Offset is the offset of the track AFTER
        // the final track in this batch.
        public long Offset {get;}
        public Batch(List<Track> tracks, long offset) 
        {
            this.Tracks = tracks;
            this.Offset = offset;
        }

        public Batch(Raw.Batch rawBatch) 
        {
            this.Offset = extractOffset(rawBatch.next_href);
            this.Tracks = new List<Track>();

            foreach (Raw.Collection collection in rawBatch.collection)
            {
                this.Tracks.Add(
                    new Track(
                        collection.track.id,
                        collection.track.title,
                        collection.track.permalink_url
                    )
                );
            }
        }

        public bool IsEmpty()
        {
            return this.Tracks == null || this.Tracks.Count <= 0;
        }

        private static long extractOffset(string url)
        {
            if (url == null) {
                return 999999999999999;
            } 

            return Convert.ToInt64(url.Split(new char[] {'=', '&'})[1]);
        }
    }
}