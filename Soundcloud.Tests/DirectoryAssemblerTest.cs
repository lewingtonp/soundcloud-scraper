using System;
using System.IO;
using NUnit.Framework;
using Soundcloud;
using System.Net.Http;
using System.Collections.Generic;
using System.Runtime.Serialization.Json;


namespace Soundcloud.UnitTests
{   
    // dotnet test --filter FullyQualifiedName~Soundcloud.UnitTests.DirectoryAssemblerAssemblesFiles
    [TestFixture]
    public class DirectoryAssemblerAssemblesFiles
    {
        private DirectoryAssembler _Assembler;
        private string _SaveDirectory;
        private string _ClientID;
        private HttpClient _Client;
        private BatchExtractor _BatchExtractor;
        [SetUp]
        public void InitializeVars()
        {   
            this._SaveDirectory = $"{Constants.ProjectPath}Soundcloud.Tests/dummy";
            this._ClientID = Constants.ClientID;
            this._Client = new HttpClient();
            this._BatchExtractor = new BatchExtractor();

            foreach (string path in this.savedFiles()) 
            {
                File.Delete(path);
            }

            int fileCount = this.savedFiles().Length;

            if (fileCount != 0)
            {
                throw new Exception($"the savedirectory {this._SaveDirectory} was not empty at the start of the test");
            }

        }

        [Test]
        public void TestAssemblesDummyFile()
        {
            this._Assembler = new DirectoryAssembler(this._SaveDirectory, this._ClientID, this._Client);
            Batch batch = this._BatchExtractor.Extract("small-batch.json");
            this._Assembler.AssembleTracks(batch);
            string[] savedFiles = this.savedFiles();
            Array.Sort(savedFiles);
            Assert.AreEqual(2, savedFiles.Length);
            StringAssert.AreEqualIgnoringCase($"{Constants.ProjectPath}Soundcloud.Tests/dummy/PREMIERE: Eeemus - Cellar Door (Steve Bleas Remix) [Recovery Collective].mp3", savedFiles[0]);
            StringAssert.AreEqualIgnoringCase($"{Constants.ProjectPath}Soundcloud.Tests/dummy/Premiere: Golan Zocher - The Element (Nick Muir Remix) [Strange Town Recordings].mp3", savedFiles[1]);
        }

        private string[] savedFiles() 
        {
            return Directory.GetFiles(this._SaveDirectory, "*", SearchOption.AllDirectories);
        }
    }
}