using System;
using NUnit.Framework;
using Soundcloud;
using System.Net.Http;
using System.Collections.Generic;


namespace Soundcloud.UnitTests
{   
    [TestFixture]
    public class DirectLocatorRetrivesTracks
    {
        private DirectLocator _Locator;
        private int _UserID;
        private int _AppVersion;
        private string _ClientID;
        private HttpClient _Client;
        [SetUp]
        public void InitializeVars()
        {
            this._UserID = Constants.UserID;
            this._AppVersion = Constants.AppVersion;
            this._ClientID = Constants.ClientID;
            this._Client = new HttpClient();
        }

        [Test]
        public void TestMakesRequest()
        {
            if (Environment.GetEnvironmentVariable("TESTTYPE") == "local") {
                Assert.Ignore();
            }

            this._Locator = new DirectLocator(this._UserID, this._AppVersion, this._ClientID, this._Client);
            Batch newBatch = this._Locator.NextBatch(0);
            Assert.AreEqual(200, newBatch.Tracks.Count);
            Assert.Greater(newBatch.Offset,1263780);
        }
    }
}