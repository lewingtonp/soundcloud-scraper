using NUnit.Framework;
using Soundcloud;
using System.Collections.Generic;


namespace Soundcloud.UnitTests
{   
    // dotnet test --filter FullyQualifiedName~Soundcloud.UnitTests.LinkMatchOverseerIdentifiesFinalBatch
    [TestFixture]
    public class LinkMatchOverseerIdentifiesFinalBatch
    {
        private LinkMatchOverseer _Overseer;
        private List<Track> _Tracks;
        private Batch _Batch;
        private BatchExtractor _BatchExtractor;

        [SetUp]
        public void InitializeVars()
        {
            this._Tracks = new List<Track>(new Track[] {});
            this._Batch = new Batch(this._Tracks, 12381239);
            this._BatchExtractor = new BatchExtractor();
        }

        [Test]
        public void TestCorrectlyIdentifiesLink()
        {
            var link = "https://soundcloud.best-beats-2018";
            _Overseer = new LinkMatchOverseer(link);
            this._Batch.Tracks.Add(new Track(12343213, "Best beats 2018", "something"));
            Assert.IsFalse(_Overseer.FinalBatch(this._Batch));
            this._Batch.Tracks.Add(new Track(2137123, "Best beats 2018", "something else"));
            Assert.IsFalse(_Overseer.FinalBatch(this._Batch));

            this._Batch.Tracks.Add(new Track(3627123, "Best beats 2018", link+"asdasdd"));
            Assert.IsFalse(_Overseer.FinalBatch(this._Batch));

            this._Batch.Tracks.Add(new Track(2137199, "Best beats 2018", link));
            Assert.IsTrue(_Overseer.FinalBatch(this._Batch));
        }

        [Test]
        public void TestCorrectlyIdentifiesEmptyBatch()
        {
            this._Overseer = new LinkMatchOverseer("non existant link");
            this._BatchExtractor = new BatchExtractor();
            Batch smallBatch = this._BatchExtractor.Extract("small-batch.json");
            Assert.IsFalse(this._Overseer.FinalBatch(smallBatch));

            Batch emptyBatch = this._BatchExtractor.Extract("empty-batch.json");
            Assert.IsTrue(this._Overseer.FinalBatch(emptyBatch));
        }

    }
}