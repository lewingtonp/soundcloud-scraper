using System;
using System.IO;
using System.Runtime.Serialization.Json;


namespace Soundcloud.UnitTests
{
    class BatchExtractor
    {
        private string batchDirectoryPath;

        public BatchExtractor()
        {
            this.batchDirectoryPath = $"{Constants.ProjectPath}/Soundcloud.Tests/resources/";
        }

        public Batch Extract(string fileName) {
            string batchFilePath = this.batchDirectoryPath + fileName;
            using (Stream fileContents = File.Open(batchFilePath, FileMode.Open))
            {
                DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(Raw.Batch));
                Raw.Batch rawBatch = (Raw.Batch)deserializer.ReadObject(fileContents);
                return new Batch(rawBatch);
            } 
        }
    }
}