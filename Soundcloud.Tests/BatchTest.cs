using NUnit.Framework;
using Soundcloud;
using System.Collections.Generic;

namespace Soundcloud.UnitTests
{
    public class BatchInitialization
    {
        private BatchExtractor _BatchExtractor;

        [SetUp]
                public void InitializeVars()
        {
            this._BatchExtractor = new BatchExtractor();
        }

        [Test]
        public void TestBatchOffsetFromRawBatch()
        {   
            long nextOffset = 1556508367884611;
            Raw.Batch rawBatch = new Raw.Batch();
            rawBatch.collection = new List<Raw.Collection>();
            rawBatch.next_href = string.Format($"https://api-v2.soundcloud.com/users/317883075/track_likes?offset={nextOffset}&limit=24");


            Batch batch = new Batch(rawBatch);
            Assert.AreEqual(nextOffset, batch.Offset);

            nextOffset = 1556508362434611;
            rawBatch.next_href = string.Format($"https://api-v2.soundcloud.com/users/317883075/track_likes?offset={nextOffset}&limit=24");
            batch = new Batch(rawBatch);
            Assert.AreEqual(nextOffset, batch.Offset);
        }

        [Test]
        public void TestBatchTracksFromRawBatch()
        {   
            Raw.Batch rawBatch = new Raw.Batch();
            rawBatch.collection = new List<Raw.Collection>();
            rawBatch.next_href = "https://api-v2.soundcloud.com/users/317883075/track_likes?offset=1723891273&limit=24";


            Batch batch = new Batch(rawBatch);
            CollectionAssert.IsEmpty(batch.Tracks);
            
            Raw.Collection col = new Raw.Collection();
            Raw.Track trk = new Raw.Track();
            string permalink = "asdj8asdu98";
            string title = "xxxxxx";
            trk.permalink_url = permalink;
            trk.title = title;
            col.track = trk;
            rawBatch.collection.Add(
                col
            );
            batch = new Batch(rawBatch);
            CollectionAssert.IsNotEmpty(batch.Tracks);
            Assert.AreEqual(1, batch.Tracks.Count);
            StringAssert.AreEqualIgnoringCase(title, batch.Tracks[0].Name);
            StringAssert.AreEqualIgnoringCase(permalink, batch.Tracks[0].Link);

            rawBatch.collection.Add(
                col
            );
            batch = new Batch(rawBatch);
            Assert.AreEqual(2, batch.Tracks.Count);
        }

        [Test]
        public void TestBatchIdentifiesEmptyBatch()
        {
            Assert.IsFalse(this._BatchExtractor.Extract("small-batch.json").IsEmpty());
            Assert.IsTrue(this._BatchExtractor.Extract("empty-batch.json").IsEmpty());

        }

    }
} 