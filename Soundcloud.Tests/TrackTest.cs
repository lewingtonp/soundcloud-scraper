using NUnit.Framework;
using Soundcloud;

namespace Soundcloud.UnitTests.Task
{
    public class TaskInitialization
    {
        [Test]
        public void TestNewTrackFromRawTrack()
        {   
            string permalink = "https://soundcloud.com/principles-of-geometry/dam-aicoab";
            string title = "dam aicoab";
            Raw.Track rawTrack = new Raw.Track();
            rawTrack.permalink_url =  permalink;
            rawTrack.title = title;

            Track trk = new Track(rawTrack);

            StringAssert.AreEqualIgnoringCase(permalink, trk.Link);
            StringAssert.AreEqualIgnoringCase(title, trk.Name);
        }

        [Test]
        public void TestTrackNameReplacements()
        {   
           Track trk = new Track(123, "apple~bottom", "somelink");
           StringAssert.AreEqualIgnoringCase("apple-bottom", trk.Name);

           trk = new Track(123, "&apple botto//m", "somelink");
           StringAssert.AreEqualIgnoringCase("-apple botto--m", trk.Name);
           
           trk = new Track(123, "the |%'\"nam\\e", "somelink");
           StringAssert.AreEqualIgnoringCase("the ----nam-e", trk.Name);
        }
    }
} 